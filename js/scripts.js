jQuery.noConflict();
jQuery(document).ready(function($){

	$('html').addClass('js');

	// calendar
	$('table.calendar tbody').each(function(){
		var rows = $('tr',this).length-1;
		$(this).addClass('rows__'+rows);
	});

	// popup
	$('.popup').each(function(){
		$(this).prepend('<span class="overlay" data-popup="close"/>');
		$('[data-popup=close]',this).on('click',function(e){
			e.preventDefault();
			$(this).parents('.popup').removeClass('on');
			$('html').removeClass('popup_active');
		});
		$('[data-popup=open]').on('click',function(e){
			e.preventDefault();
			var id = $(this).data('id');
			$('#'+id+'.popup').addClass('on');
			$('html').addClass('popup_active');
		})
	});

	// go top
	var go_top = function(){
		var offset = $(window).scrollTop();
		if(offset>100){
			$('#gotop').addClass('on');
		} else {
			$('#gotop').removeClass('on');
		}
	}
	go_top();
	$(window).scroll(function(){
		go_top();
	});
	$('#gotop a').click(function(){ 
		$('html,body').animate({scrollTop:0},600);
		return false;
	});

	// file input style
	$('form.normal input[type=file]').each(function(){var userLang=(navigator.language)?navigator.language:navigator.userLanguage;var uploadText='Vyberte soubor';if(userLang=='en'){var uploadText='Choose file'}if(userLang=='de'){var uploadText='Datei ausw&auml;hlen'}if(userLang=='pl'){var uploadText='Wybierz plik'}if(userLang=='fr'){var uploadText='Choisir un fichier'}if(userLang=='ru'){var uploadText='Выберите файл'}if(userLang=='es'){var uploadText='Elegir archivo'}var uploadbutton='<input type="button" class="normal-button" value="'+uploadText+'" />';var inputClass=$(this).attr('class');$(this).wrap('<span class="fileinputs"></span>');$(this).parent().append($('<span class="fakefile" />').append($('<input class="input-file-text" type="text" />').attr({'id':$(this).attr('id')+'__fake','class':$(this).attr('class')+' input-file-text'})).append(uploadbutton));$(this).addClass('type-file').css('opacity',0);$(this).bind('change mouseout',function(){$('#'+$(this).attr('id')+'__fake').val($(this).val().replace(/^.+\\([^\\]*)$/,'$1'))})});

	if (document.createElement('input').placeholder==undefined){
		// placeholder
		$('[placeholder]').focus(function(){var input=$(this);if(input.val()==input.attr('placeholder')){input.val('');input.removeClass('placeholder')}}).blur(function(){var input=$(this);if(input.val()==''||input.val()==input.attr('placeholder')){input.addClass('placeholder');input.val(input.attr('placeholder'))}}).blur()
	}

});